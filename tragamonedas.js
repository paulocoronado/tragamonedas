var jugadas = 0;

function iniciar_maquina() {

    //Crear un objeto que referencia al elemento HTML
    let miPantalla = document.getElementById("pantalla");

    //Crear tres elementos HTML

    for (let i = 0; i < 3; i++) {
        let miDivision = document.createElement("div");
        miDivision.setAttribute("id", "figura" + (i + 1))
        miDivision.setAttribute("class", "cuadro")
        //Agregar el elemento creado al elemento que yo desee
        miPantalla.appendChild(miDivision);
    }

    let otraDivision = document.createElement("div");
    otraDivision.setAttribute("class", "divBoton");
    miPantalla.appendChild(otraDivision);


    //Crear una división poara cer el premio
    let premioDivision = document.createElement("div");
    premioDivision.setAttribute("id", "divPremio");
    premioDivision.setAttribute("class", "divPremio");
    miPantalla.appendChild(premioDivision);

    let miBoton = document.createElement("input");
    miBoton.setAttribute("type", "button");
    miBoton.setAttribute("class", "boton");
    miBoton.setAttribute("value", "Jugar");
    miBoton.setAttribute("onclick", "lanzar_jugada()");
    otraDivision.appendChild(miBoton);
}


function realizar_jugada() {

    let t = [
        "Patilla", "siete", "uvas", "diamantes", "naranja", "vacío",  "campana", "cereza",
        "euro", "peso", "bolivar", "limón", "bar", "bar2", "bar3", "vacío", "herradura", "oliva", "trébol", "vacío",
         "vacío", "manzana", "monedas", "corazón", "vacío"];
    let r = [0, 0, 0];

    if (jugadas == 10) {
        jugadas = 0;
        r = ["bolivar", "bolivar", "bolivar"];
    } else {

        for (total_selecciones = 0; total_selecciones < 3; total_selecciones++) {

            let indice = Math.round(Math.random() * (t.length - 1));
            r[total_selecciones] = t[indice];
        }
    }

    return r;
}

function lanzar_jugada() {

    let miJugada = realizar_jugada();

    //Crear un objeto que referencie al primer "contenedor"

    let miCuadro = document.getElementById("figura1");
    miCuadro.innerHTML = miJugada[0];

    miCuadro = document.getElementById("figura2");
    miCuadro.innerHTML = miJugada[1];

    miCuadro = document.getElementById("figura3");
    miCuadro.innerHTML = miJugada[2];

    jugadas++;
    console.log(jugadas);

    let miPremio=calcular_premio(miJugada);
    let miDivPremio=document.getElementById("divPremio");
    miDivPremio.innerHTML=miPremio;

}

function calcular_premio(jugada) {

    let resultado=0;

    let premios = [
        ["Patilla", "Patilla", "Patilla", 3],
        ["siete", "siete", "siete", 30],
        ["uvas", "uvas", "uvas", 1],
        ["diamantes", "diamantes", "diamantes", 20],
        ["naranja", "naranja", "naranja", 15],
        ["campana", "campana", "campana", 14],
        ["cereza", "cereza", "cereza", 10],
        ["euro", "euro", "euro", 5],
        ["peso", "peso", "peso", 8],
        ["bolivar", "bolivar", "bolivar", 1],
        ["limón", "limón", "limón", 7],
        ["bar", "bar", "bar", 3],
        ["bar2", "bar2", "bar2", 6],
        ["bar3", "bar3", "bar3", 9],
        ["herradura", "herradura", "herradura", 10],
        ["oliva", "oliva", "oliva", 5],
        ["trébol", "trébol", "trébol", 21],
        ["manzana", "manzana", "manzana", 5],
        ["monedas", "monedas", "monedas", 12],
        ["corazón", "corazón", "corazón", 15],
        ["naranja", "limón", "manzana", 3],
        ["euro", "peso", "bolivar", 1]
    ];

    for (let i=0; i < premios.length; i++){

        if(jugada[0]===premios[i][0] && jugada[1]===premios[i][1] && jugada[2]===premios[i][2]){

            resultado=premios[i][3];

        }else if(jugada[0]==="siete"){

            resultado=3;
        }else if(jugada[0]=="bar3"){
            resultado=2;
        }
    }

    return resultado;

}

